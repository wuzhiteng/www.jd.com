<?php
// 让浏览器识别是html语言
header('content-type:text/html;charset=utf-8');
// 让浏览器识别成json语言
// header('content-type:application/json;charset=utf-8');
// 获得数据库中的数据
$mysql_conf=array(
  // 主机名端口
  'host'=>'localhost:3306',
  // 用户名
  'user'=>'root',
  // 密码
  'pass'=>'root',
  // 数据库名
  'db'=>'users'
);
// 关联数据库
$conn=@new mysqli($mysql_conf['host'],$mysql_conf['user'],$mysql_conf['pass']);
//  var_dump($conn);//打印关联数据库是否正确
if($conn->connect_error){//判断关联数据库是否出现错误
  //die();结束进程 终止代码向下执行
  die('连接错误'.$conn->connect_errno);//终止代码执行 并输出错误代码
}
//设置查询的字符集
$conn->query('set names utf-8');
//选择自己创建的数据库 'db'=>'user'
$selected = $conn->select_db($mysql_conf['db']);
//打印选择的数据库是否正确
// var_dump($selected);
if(!$selected){//判断数据库是否选择正确
  die('数据库选择错误'.$conn->error);
}
?>