// import { Swiper } from "./swiper-bundle.esm.browser.js";
import {Swiper} from './swiper-bundle.esm.browser.js';

let mySwiper =(function(){
// 轮播图右边小轮播
var mySwiper = new Swiper('.mySwiper', {
  loop: true, // 循环模式选项
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  autoplay:{
    delay:3000
  },
});
// 主体中间轮播图
var mySwiper = new Swiper('.mySwiper2', {
  loop: true, // 循环模式选项
 navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  autoplay:{
    delay:3000
  },
   });
// 主体右边小轮播图
var mySwiper = new Swiper('.mySwiper1', {
  autoplay:{
    delay:1500
  },
  loop: true, // 循环模式选项
  // 如果需要分页器
  pagination: {
    el: '.swiper-pagination',
  },
});  
})();

export default mySwiper;