let countDown = (function () {
  // 获取时间元素
  let h = document.querySelector(".h");
  // 获取分钟元素
  let m = document.querySelector(".m");
  // 获取秒元素
  let s = document.querySelector(".s");
  var timre = setInterval(function () {
  // 设置国庆时间
  let future = new Date(2022, 9, 1, 0, 0, 0);
  // 获取现在的时间
  let now = new Date();
  // 用国庆的时间减现在的时间
  let num = future - now;
  // let sky =parseInt(num / 1000 / 60 / 60 / 24) < 10? "0" + parseInt(num / 1000 / 60 / 60 / 24): parseInt(num / 1000 / 60 / 60 / 24);
    h.innerHTML =parseInt((num / 1000 / 60 / 60) % 24) < 10? "0" + parseInt((num / 1000 / 60 / 60) % 24): parseInt((num / 1000 / 60 / 60) % 24);
    m.innerHTML =parseInt((num / 1000 / 60) % 60) < 10? "0" + parseInt((num / 1000 / 60) % 60): parseInt((num / 1000 / 60) % 60);
    s.innerHTML =parseInt((num / 1000) % 60) < 10? "0" + parseInt((num / 1000) % 60): parseInt((num / 1000) % 60);
    
  }, 1000);
})();
export default countDown;
