import $ from "./lib/jquery.esm.js";
import cookie from "./lib/cookie.js";
// 获取cookie里面的值
let shop = cookie.get("shop");
// 把获取到的cookie数据转换成数组
shop = JSON.parse(shop);

// 筛选出需要的id属性
let idList = shop.map((el) => el.id).join();
// 发送请求
$.ajax({
  type: "get", //get请求
  url: "../interface/shop.php",
  data: { idList }, //发送id属性
  dataType: "json",
})
  .then((res) => {
    // 定义空字符串
    let temp = "";
    // console.log(res);
    res.forEach((el) => {
      // 给数组排序
      let current = shop.filter((em) => em.id == el.id);
      let pic = JSON.parse(el.picture);
      // console.log(pic);
      temp += `
    <div class="box5">
        <div class="box5-top">
          <div class="box5-left">
            <div>
              <input type="checkbox"   data-id=${el.id}>
              <a  > <img src="./${
                pic[0].src
              }" style="width:80px;height:80px;"  alt=""></a>
            </div>
            <div>
              <p>${el.title}</p>
              <p>
                <img src="./img/shopping/设置.png" alt="">
                <span>选服务</span>
              </p>
            </div>
            <p>
              <span>赛博霓虹</span><br>
              <span>12GB+256GB</span>
            </p>
          </div>
          <div class="box5-right">
            <p>￥${(+el.price).toFixed(2)}</p>
            <div>
              <ul>
                <li>-</li>
                <li>${current[0].num}</li>
                <li>+</li>
                <span>有货</span>
              </ul>
            </div>
            <p class="weight">￥${(el.price * current[0].num).toFixed(2)}</p>
            <p class="active">
              <span id="remove" data-id="${el.id}">删除</span>
              <br>
              <span>移入关注</span>
            </p>
          </div>
        </div>
        <div class="box5-bottom">
          <p>
            【赠品】体验卡免费领，多款APP免流，超大流量任性用，点击领取
          </p>
          <p>
            <span>x1</span>
            <span>查看价格</span>
          </p>
        </div>
      </div>
    `;
    });
    $(".box4").after(temp);
    $(".box5 #remove").on("click", function () {
      // 筛选出点击之外的cookie数据
      let res = shop.filter((el) => el.id != $(this).attr("data-id"));
      // 把筛选出来的数据覆盖原先的cooki属性
      cookie.set("shop", JSON.stringify(res));
      // 刷新页面重新渲染页面
      location.reload();
    });
    // 选中所有的复选框设置点击事件
    let arr = [];
    let allcheckbox = $(":checkbox");
    allcheckbox.on("click", function () {
      allPrice();
      if (arr.length == shop.length) {
        $(":checkbox").prop("checked", $(this).prop('checked'));
      } else{
        $(".allcheckbox1").prop("checked", false);
      }
    });
    // 封装一个函数 计算全部价格
    function allPrice() {
      // 每次点击清空前一个数组
      arr = [];
      // 把选中的元素添加进数组
      let allchecked = $("input:checked");
      for (let i = 0; i < allchecked.length; i++) {
        let id = $(allchecked[i]).attr("data-id");
        arr.push(id);
      }
      arr = arr.filter((el) => el);
      //  判断如果arr的长度大于等于shop数组的长度则设置全选
      // 设置选中商品的数量
      $(".preNum").html(arr.length);
      // 定义变量存放总价格
      let res = 0;
      // 遍历cookie数剧转换的数组
      shop.forEach((el) => {
        let id = el.id;
        // console.log(id);
        // 查找选中的和cookie数组对比获取价格
        if (arr.indexOf(id) > -1) {
          res += el.allprice * el.num;
        }
      });
      $(".allprice").html("￥" + res);
    }
    // 点击全选选中所有元素
    $(".allcheckbox1").on("click", function () {
      if (arr.length < shop.length) {
        $(":checkbox").prop("checked", true);
        let temp = 0;
        shop.forEach((el) => {
          temp += el.allprice * el.num;
        });
        $(".preNum").html(shop.length);
        $(".allprice").html("￥" + temp);
      } else {
        $(":checkbox").prop("checked", false);
        
        $(".preNum").html("0");
        $(".allprice").html("￥0.00");
      }
    });
  })
  .catch((xhr) => {
    console.log(xhr);
  });
