import countDown from "./countDown.js";
import mySwiper from "./banner.js";
import $ from "./lib/jquery.esm.js";
let c =$('#c');
$.ajax({
  type: "get",
  url: "../interface/getitems.php",
  dataType: "json",
})
  .then((res) => {

    let template = "";
    res.forEach((el) => {
      let pic = JSON.parse(el.picture);
 
    
      template += `<a href="./details.html?id=${el.id}" class="box" style="margin-right: 0;">
      <div class="content">
        <img src="./${pic[0].src}" alt="">
        <p>${el.title}</p>
        <div><span>￥</span><i>${el.price}</i>
          <div>满减</div>
        </div>
      </div>
      <div class="mask">
        <div>
          <!-- <img src="./img/9978.png" alt=""> -->
          <span>找相似</span>
        </div>
      </div>
    </a> `;
    });
  
   c.html(template);
  })
  .catch((xhr) => {
    console.log(xhr.status);
  });
