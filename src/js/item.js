import $ from "./lib/jquery.esm.js";
import cookie from "./lib/cookie.js";
let id = location.search.split("=")[1];

$.ajax({
  type: "get",
  url: "../interface/getitem.php",
  data: { id },
  dataType: "json",
})
  .then((res) => {
    let pic = JSON.parse(res.picture);
    let template = `
  <div>
     <img src="./${pic[0].src}" alt="">
    <p>${res.title}</p>
    <div>
      <span>价格:${res.price}</span>
      <input type="number" value="1" min='1' max='99' id='num'>
      <span>数量</span>
      <input type="button" id="additem" value="加入购物车">
    </div>  
  </div>
  `;

    $(".box-details").html(template);
    // 在未来元素后面添加点击事件

    $("#additem").on("click", function () {
     
      // 把点击的商品id和数量作为参数传入进去
      addItem(res.id, $("#num").val(),res.price);
      alert('加入成功')
    });
  })
  .catch((xhr) => {
    console.log(xhr.status);
  });

// 封装函数把点击的值保存在cookie里面
function addItem(id, num,allprice) {
  //需要的保存的id和数量num
  let product = { id, num ,allprice};
  // 把已有的cooki保存下来
  let shop = cookie.get("shop");
  if (shop) {
    //判断cookie是否有初始数据
    // 把保存的初始数据改成数组
    shop=JSON.parse(shop);
    // 遍历数组如果点击的是重复商品则需要修改数量就可以   
    // 获得商品对象在数组中的索引
  let index=shop.findIndex(elm => elm.id == id);
  if(index==-1){
    shop.push(product);
  }else{
    shop[index].num=parseInt(shop[index].num)+parseInt(num);
  }

  } else {
    // 没有初始数据把shop定义成空数组 存放数据
    //  把id和数量存入到cookie当中
    shop = [];
    shop.push(product);
  }

  // 将获取的id 和 num数量 转成json格式保存在cookie中
  // console.log(shop);
  cookie.set("shop", JSON.stringify(shop));
}
