window.onload = function() {
  // 1.获取元素
  // 1-1 获取图片焦点
  const imgBox = document.querySelector(".banner-big>.box>ul");
  // console.log(imgBox);
  // 1-2获取承载焦点的ol盒子
  const olBox = document.querySelector(".banner-big>.box>ol");
  // console.log(olBox);
  // 1-3获取可视区域的盒子
  const box = document.querySelector('.banner-big>.box');
  // console.log(box);

  // 2.定义变量
  // 获取可视距离的宽度
  const box_width = box.clientWidth;
  // console.log(box_width);
  // 定义清除定时器变量
  let tirme = 0;
  // 准备一个变量记录第几张图片
  let index = 1;
  // 存取点击时的索引j
  let j = 1;
  // 3.生成焦点
  setPoint();

  function setPoint() {
      // 3-1需要生成的焦点数量
      const num = imgBox.children.length;
      // console.log(num);
      // 3-2创建若干个li放在ol里面
      // 创建文档碎片减少回流
      const fag = document.createDocumentFragment();
      for (let i = 0; i < num; i++) {
          // 3-2-1创建元素li
          const li = document.createElement("li");

          li.setAttribute('type', 'point');
          // 3-2-2默认第一个li带有active类名
          if (i === 0) li.classList.add("active");
          // 把创建的li元素添加到文档碎片里面
          fag.appendChild(li);
      }
      // 3-3把生成的焦点元素放到承载的ol盒子里卖弄
      olBox.appendChild(fag);
      // 3-3-1调正焦点盒子的宽度
      olBox.style.width = num * 20 * 2 + "px";
  }
  // 4.克隆元素
  copyEle();

  function copyEle() {
      // 复制节点
      const first = imgBox.firstElementChild.cloneNode(true);
      // console.log(first);
      const last = imgBox.lastElementChild.cloneNode(true);
      // console.log(last);
      // 插入节点
      imgBox.appendChild(first);
      // 把复制的最后一个节点插入到最前面
      imgBox.insertBefore(last, imgBox.firstChild);
      // 设置最新的图片盒子宽度
      imgBox.style.width = imgBox.children.length * 100 + '%';
      // 设置显示图片的位置
      imgBox.style.left = -box_width + 'px';
  }

  // 5.让图片自动运动
  autoplay();

  function autoplay() {

      // 改变图片显示位置
      tirme = setInterval(() => {
          // olFocus();
          // console.log(index);

         
          if (index >= imgBox.children.length - 2) {
              index = 1;
              imgBox.style.left = -index * box_width + 'px';
              // olBox.firstChild.className = 'active';

          } else {
              index++;
              imgBox.style.left = -index * box_width + 'px';
          }
          // 焦点跟着图片改变
          for (let i = 0; i < olBox.children.length; i++) {
              olBox.children[i].className = '';
          }
          // console.log(index - 1);
          olBox.children[index - 1].className = 'active';
      }, 1000);
  }

  function olFocus() {
      // console.log(index);
      if (index >= imgBox.children.length - 1) {
          index = 1;
          imgBox.style.left = -index * box_width + 'px';
          // olBox.firstChild.className = 'active';

      } else {
          // index++;
          imgBox.style.left = -index * box_width + 'px';
      }
      if (index < 1) {
          index = imgBox.children.length - 2;
      }
      // 焦点跟着图片改变
      for (let i = 0; i < olBox.children.length; i++) {
          olBox.children[i].className = '';
      }
      // console.log(index - 1);
      olBox.children[index - 1].className = 'active';
  }
  // 移入移出事件
  overOut();

  function overOut() {
      box.addEventListener('mouseover', () => clearInterval(tirme));
      box.addEventListener('mouseout', () => autoplay());
  }
  // 6.设置点击事件
  boxclilk();

  function boxclilk() {
      box.addEventListener('click', ev => {
          // console.log(ev);
          ev = ev || window.event;
          // const target = ev.target || ev.srcElement;
          // 点击右边切换图片
          if (ev.target.className === 'right') {
              index++;
              olFocus();
              imgBox.children.classList('animate__heartBeat');
          } else if (ev.target.className === 'left') {
              // 点击左边切换图片
              index--;
              olFocus();
          } else if (ev.target.type === 'point') {
              console.log(imgBox.children);
              for (let i = 0; i < olBox.children.length; i++) {
                  olBox.children[i].className = '';
              }
              ev.target.className = 'active';
             
              let arr = Array.from(olBox.children);
              // console.log(arr);
              let j = arr.findIndex(el => {
                  return el.getAttribute('class') === 'active';
              });
              // console.log(index);
              imgBox.style.left = -(j + 1) * box_width + 'px';
              index = j + 1;
          }
      })
  }

};